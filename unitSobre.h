//---------------------------------------------------------------------------

#ifndef unitSobreH
#define unitSobreH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>
//---------------------------------------------------------------------------
class TfrmSobre : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TBevel *Bevel1;
        TLabel *lblYam;
        TLabel *lblVersao;
        TLabel *lblDesenvolvido;
        TLabel *lblEmail;
        TLabel *lblMailto;
        TButton *btnOk;
        void __fastcall lblMailtoClick(TObject *Sender);
        void __fastcall btnOkClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmSobre(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmSobre *frmSobre;
//---------------------------------------------------------------------------
#endif
