//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "unitCliente.h"
#include "unitYam.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmCliente *frmCliente;
//---------------------------------------------------------------------------
__fastcall TfrmCliente::TfrmCliente(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------

void __fastcall TfrmCliente::btnConectarClick(TObject *Sender)
{
 if (edNome->Text=="")
  Application->MessageBoxA("Voc� deve digitar um Nome!","Erro!",MB_OK);
 else
  if (edIP->Text!="" && edPorta->Text!="")
  {
   frmYam->ClientSocket->Address=edIP->Text;
   frmYam->ClientSocket->Port=StrToInt(edPorta->Text);
   frmYam->ClientSocket->Open();
  }
  else
   Application->MessageBoxA("Voc� deve digitar um IP e uma Porta!","Erro!",MB_OK);
}
//---------------------------------------------------------------------------

