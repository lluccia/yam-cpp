//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("unitYam.cpp", frmYam);
USEFORM("unitSobre.cpp", frmSobre);
USEFORM("unitJogadores.cpp", frmJogadores);
USEFORM("unitRecordes.cpp", frmRecordes);
USEFORM("unitCliente.cpp", frmCliente);
USEFORM("unitServidor.cpp", frmServidor);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
        try
        {
                 Application->Initialize();
                 Application->Title = "Yam";
                 Application->CreateForm(__classid(TfrmYam), &frmYam);
                 Application->CreateForm(__classid(TfrmSobre), &frmSobre);
                 Application->CreateForm(__classid(TfrmJogadores), &frmJogadores);
                 Application->CreateForm(__classid(TfrmRecordes), &frmRecordes);
                 Application->CreateForm(__classid(TfrmCliente), &frmCliente);
                 Application->CreateForm(__classid(TfrmServidor), &frmServidor);
                 Application->Run();
        }
        catch (Exception &exception)
        {
                 Application->ShowException(&exception);
        }
        catch (...)
        {
                 try
                 {
                         throw Exception("");
                 }
                 catch (Exception &exception)
                 {
                         Application->ShowException(&exception);
                 }
        }
        return 0;
}
//---------------------------------------------------------------------------
