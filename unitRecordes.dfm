object frmRecordes: TfrmRecordes
  Left = 357
  Top = 241
  BorderStyle = bsDialog
  Caption = 'Recordes'
  ClientHeight = 230
  ClientWidth = 183
  Color = clMoneyGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 8
    Width = 28
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 136
    Top = 8
    Width = 24
    Height = 13
    Caption = 'Total'
  end
  object btnOk: TButton
    Left = 56
    Top = 200
    Width = 75
    Height = 25
    Caption = '&Ok'
    TabOrder = 0
    OnClick = btnOkClick
  end
  object lbNome: TListBox
    Left = 8
    Top = 24
    Width = 113
    Height = 169
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 1
  end
  object lbTotal: TListBox
    Left = 120
    Top = 24
    Width = 57
    Height = 169
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 2
  end
end
