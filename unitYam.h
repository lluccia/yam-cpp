//---------------------------------------------------------------------------

#ifndef unitYamH
#define unitYamH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ImgList.hpp>
#include <Graphics.hpp>
#include <Grids.hpp>
#include <ComCtrls.hpp>
#include <ToolWin.hpp>
#include <Menus.hpp>
#include <MPlayer.hpp>
#include <NMMSG.hpp>
#include <Psock.hpp>
#include <ScktComp.hpp>
//---------------------------------------------------------------------------
class TfrmYam : public TForm
{
__published:	// IDE-managed Components
        TButton *btnJogar;
        TImageList *imglistDados;
        TImageList *imglistDadosL;
        TImage *Cartela;
        TImage *Dados;
        TLabel *Label4;
        TMainMenu *MainMenu;
        TMenuItem *mJogo;
        TMenuItem *mAjuda;
        TMenuItem *mNovoJogo;
        TMenuItem *mSair;
        TMenuItem *N1;
        TMenuItem *Sobre;
        TMenuItem *Regras;
        TMenuItem *N2;
        TLabel *lblJogadas;
        TLabel *Label2;
        TLabel *lbTotalNosDados;
        TLabel *Label5;
        TLabel *lblJogador1;
        TLabel *lblJogador2;
        TLabel *lblJogador3;
        TLabel *lblJogador4;
        TLabel *lbJogadas;
        TLabel *lbJogador1;
        TLabel *lbJogador2;
        TLabel *lbJogador3;
        TLabel *lbJogador4;
        TMenuItem *N0;
        TMenuItem *mRecordes;
        TMemo *memoChat;
        TEdit *edChat;
        TButton *btnEnviar;
        TLabel *lbChat;
        TClientSocket *ClientSocket;
        TServerSocket *ServerSocket;
        TMenuItem *mMultiplayer;
        TMenuItem *mServidor;
        TMenuItem *mCliente;
        void __fastcall btnJogarClick(TObject *Sender);
        void __fastcall DadosMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall CartelaMouseDown(TObject *Sender,
          TMouseButton Button, TShiftState Shift, int X, int Y);
        void __fastcall mNovoJogoClick(TObject *Sender);
        void __fastcall mSairClick(TObject *Sender);
        void __fastcall SobreClick(TObject *Sender);
        void __fastcall RegrasClick(TObject *Sender);
        void __fastcall mRecordesClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall edChatKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall mServidorClick(TObject *Sender);
        void __fastcall mClienteClick(TObject *Sender);
        void __fastcall ClientSocketConnect(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerSocketListen(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientSocketConnecting(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ServerSocketClientRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall ClientSocketRead(TObject *Sender,
          TCustomWinSocket *Socket);
        void __fastcall btnEnviarClick(TObject *Sender);
        void __fastcall ClientSocketDisconnect(TObject *Sender,
          TCustomWinSocket *Socket);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmYam(TComponent* Owner);
        void TfrmYam::Iniciar();
        void TfrmYam::JogaDados();
        void TfrmYam::OrdenaDados();
        void TfrmYam::DesenhaDados();
        void TfrmYam::Combinacoes();
        void TfrmYam::MarcacoesPossiveis();
        void TfrmYam::LimpaCartela();
        void TfrmYam::DesenhaPontos();
        void TfrmYam::CalculaTotais();
        bool TfrmYam::ChecaFimDeJogo();
        void TfrmYam::FimDeJogo();
        void TfrmYam::ZeraScore();
        void TfrmYam::MarcaPonto(int i,int j);
        void TfrmYam::ChecaRecordes();
        void TfrmYam::HLJogador();
        void TfrmYam::IniciarMultiplayer();        
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmYam *frmYam;
//---------------------------------------------------------------------------
#endif
