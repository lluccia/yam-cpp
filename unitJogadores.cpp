//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "unitJogadores.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmJogadores *frmJogadores;

//---------------------------------------------------------------------------
__fastcall TfrmJogadores::TfrmJogadores(TComponent* Owner)
        : TForm(Owner)
{

}
//---------------------------------------------------------------------------

void __fastcall TfrmJogadores::udJogadoresClick(TObject *Sender,
      TUDBtnType Button)
{
 Edit2->Visible = false;
 Edit3->Visible = false;
 Edit4->Visible = false;

 if (udJogadores->Position > 1)
 {
  Edit2->Visible = true;
  if (udJogadores->Position > 2)
  {
   Edit3->Visible = true;
   if (udJogadores->Position > 3)
   {
    Edit4->Visible = true;
   }
  }
 }
}
//---------------------------------------------------------------------------

