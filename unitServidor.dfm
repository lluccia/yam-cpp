object frmServidor: TfrmServidor
  Left = 220
  Top = 204
  BorderStyle = bsDialog
  Caption = 'Multiplayer - Servidor'
  ClientHeight = 154
  ClientWidth = 233
  Color = clMoneyGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 24
    Top = 44
    Width = 28
    Height = 13
    Caption = 'Nome'
  end
  object Label2: TLabel
    Left = 28
    Top = 11
    Width = 25
    Height = 13
    Caption = 'Porta'
  end
  object Label4: TLabel
    Left = 8
    Top = 92
    Width = 49
    Height = 13
    Caption = 'Jogadores'
  end
  object btnIniciar: TButton
    Left = 152
    Top = 104
    Width = 75
    Height = 25
    Caption = '&Iniciar'
    Enabled = False
    ModalResult = 1
    TabOrder = 3
  end
  object edNome: TEdit
    Left = 64
    Top = 40
    Width = 81
    Height = 21
    MaxLength = 9
    TabOrder = 1
  end
  object lbJogadores: TListBox
    Left = 64
    Top = 72
    Width = 81
    Height = 57
    TabStop = False
    ItemHeight = 13
    TabOrder = 4
  end
  object sbServidor: TStatusBar
    Left = 0
    Top = 135
    Width = 233
    Height = 19
    Color = clMoneyGreen
    Panels = <>
    SimplePanel = True
  end
  object btnCriar: TButton
    Left = 152
    Top = 72
    Width = 75
    Height = 25
    Caption = '&Criar Jogo'
    TabOrder = 2
    OnClick = btnCriarClick
  end
  object edPorta: TEdit
    Left = 64
    Top = 8
    Width = 41
    Height = 21
    TabOrder = 0
    Text = '6980'
  end
end
