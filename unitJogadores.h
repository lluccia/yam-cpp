//---------------------------------------------------------------------------

#ifndef unitJogadoresH
#define unitJogadoresH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmJogadores : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *edJogadores;
        TUpDown *udJogadores;
        TBevel *Bevel1;
        TButton *btnOk;
        TLabel *Label2;
        TEdit *Edit1;
        TEdit *Edit2;
        TEdit *Edit3;
        TEdit *Edit4;
        TLabel *Label3;
        void __fastcall udJogadoresClick(TObject *Sender,
          TUDBtnType Button);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmJogadores(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmJogadores *frmJogadores;
//---------------------------------------------------------------------------
#endif
