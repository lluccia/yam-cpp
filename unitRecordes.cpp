//---------------------------------------------------------------------------

#include <vcl.h>
#include <stdio.h>
#include <process.h>
#pragma hdrstop

#include "unitRecordes.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmRecordes *frmRecordes;

 struct Recorde
 {
  char Nome[10];
  int Score;
 } Reco[10];

//---------------------------------------------------------------------------
__fastcall TfrmRecordes::TfrmRecordes(TComponent* Owner)
        : TForm(Owner)
{
}



void __fastcall TfrmRecordes::btnOkClick(TObject *Sender)
{

 frmRecordes->Close();
}
//---------------------------------------------------------------------------

void __fastcall TfrmRecordes::FormShow(TObject *Sender)
{
 FILE* fp;
 lbNome->Clear();
 lbTotal->Clear();

 if((fp=fopen("high.dat","rb"))==NULL)
 {
  exit(1);
 }
 if((fread(Reco,sizeof(Reco),1,fp))!=1)
 {
  exit(1);
 }
 fclose(fp);

 for(int c=0;c<10;c++)
 {
  lbNome->AddItem(IntToStr(c+1)+"."+Reco[c].Nome,NULL);
  lbTotal->AddItem(Reco[c].Score,NULL);
 }
}
//---------------------------------------------------------------------------

