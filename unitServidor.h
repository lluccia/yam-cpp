//---------------------------------------------------------------------------

#ifndef unitServidorH
#define unitServidorH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmServidor : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label3;
        TLabel *Label2;
        TLabel *Label4;
        TButton *btnIniciar;
        TEdit *edNome;
        TListBox *lbJogadores;
        TStatusBar *sbServidor;
        TButton *btnCriar;
        TEdit *edPorta;
        void __fastcall btnCriarClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmServidor(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmServidor *frmServidor;
//---------------------------------------------------------------------------
#endif
