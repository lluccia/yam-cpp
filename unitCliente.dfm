object frmCliente: TfrmCliente
  Left = 344
  Top = 289
  BorderStyle = bsDialog
  Caption = 'Multiplayer - Cliente'
  ClientHeight = 154
  ClientWidth = 235
  Color = clMoneyGreen
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 24
    Top = 44
    Width = 28
    Height = 13
    Caption = 'Nome'
  end
  object Label4: TLabel
    Left = 8
    Top = 92
    Width = 49
    Height = 13
    Caption = 'Jogadores'
  end
  object Label1: TLabel
    Left = 24
    Top = 12
    Width = 10
    Height = 13
    Caption = 'IP'
  end
  object Label2: TLabel
    Left = 152
    Top = 12
    Width = 25
    Height = 13
    Caption = 'Porta'
  end
  object edNome: TEdit
    Left = 64
    Top = 40
    Width = 81
    Height = 21
    MaxLength = 9
    TabOrder = 2
  end
  object lbJogadores: TListBox
    Left = 64
    Top = 72
    Width = 81
    Height = 57
    TabStop = False
    ItemHeight = 13
    TabOrder = 4
  end
  object sbCliente: TStatusBar
    Left = 0
    Top = 135
    Width = 235
    Height = 19
    Color = clMoneyGreen
    Panels = <>
    SimplePanel = True
  end
  object btnConectar: TButton
    Left = 152
    Top = 104
    Width = 75
    Height = 25
    Caption = '&Conectar'
    TabOrder = 3
    OnClick = btnConectarClick
  end
  object edPorta: TEdit
    Left = 184
    Top = 8
    Width = 41
    Height = 21
    TabOrder = 1
    Text = '6980'
  end
  object edIP: TEdit
    Left = 40
    Top = 8
    Width = 105
    Height = 21
    TabOrder = 0
  end
end
