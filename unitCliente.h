//---------------------------------------------------------------------------

#ifndef unitClienteH
#define unitClienteH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
//---------------------------------------------------------------------------
class TfrmCliente : public TForm
{
__published:	// IDE-managed Components
        TEdit *edNome;
        TListBox *lbJogadores;
        TLabel *Label3;
        TStatusBar *sbCliente;
        TButton *btnConectar;
        TLabel *Label4;
        TLabel *Label1;
        TLabel *Label2;
        TEdit *edPorta;
        TEdit *edIP;
        void __fastcall btnConectarClick(TObject *Sender);

private:	// User declarations
public:		// User declarations
        __fastcall TfrmCliente(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmCliente *frmCliente;
//---------------------------------------------------------------------------
#endif
