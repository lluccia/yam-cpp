object frmJogadores: TfrmJogadores
  Left = 384
  Top = 319
  BorderStyle = bsDialog
  Caption = 'Jogadores'
  ClientHeight = 159
  ClientWidth = 256
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 169
    Height = 110
  end
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 155
    Height = 13
    Alignment = taCenter
    Caption = 'Escolha o n'#250'mero de Jogadores:'
  end
  object Label2: TLabel
    Left = 64
    Top = 32
    Width = 57
    Height = 13
    Alignment = taCenter
    Caption = '(entre 1 e 4)'
  end
  object Label3: TLabel
    Left = 184
    Top = 8
    Width = 36
    Height = 13
    Caption = 'Nomes:'
  end
  object edJogadores: TEdit
    Left = 64
    Top = 64
    Width = 33
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 0
    Text = '1'
  end
  object udJogadores: TUpDown
    Left = 97
    Top = 64
    Width = 24
    Height = 24
    Associate = edJogadores
    Min = 1
    Max = 4
    Position = 1
    TabOrder = 1
    Wrap = False
    OnClick = udJogadoresClick
  end
  object btnOk: TButton
    Left = 88
    Top = 128
    Width = 75
    Height = 25
    Caption = 'Ok'
    Default = True
    ModalResult = 1
    TabOrder = 2
  end
  object Edit1: TEdit
    Left = 184
    Top = 24
    Width = 65
    Height = 21
    MaxLength = 9
    TabOrder = 3
    Text = 'Jogador 1'
  end
  object Edit2: TEdit
    Left = 184
    Top = 48
    Width = 65
    Height = 21
    MaxLength = 9
    TabOrder = 4
    Text = 'Jogador 2'
    Visible = False
  end
  object Edit3: TEdit
    Left = 184
    Top = 72
    Width = 65
    Height = 21
    MaxLength = 9
    TabOrder = 5
    Text = 'Jogador 3'
    Visible = False
  end
  object Edit4: TEdit
    Left = 184
    Top = 96
    Width = 65
    Height = 21
    MaxLength = 9
    TabOrder = 6
    Text = 'Jogador 4'
    Visible = False
  end
end
