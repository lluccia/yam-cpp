//---------------------------------------------------------------------------

#ifndef unitRecordesH
#define unitRecordesH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
//---------------------------------------------------------------------------
class TfrmRecordes : public TForm
{
__published:	// IDE-managed Components
        TButton *btnOk;
        TListBox *lbNome;
        TListBox *lbTotal;
        TLabel *Label1;
        TLabel *Label2;
        void __fastcall btnOkClick(TObject *Sender);
        void __fastcall FormShow(TObject *Sender);
private:	// User declarations
public:		// User declarations
        __fastcall TfrmRecordes(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TfrmRecordes *frmRecordes;
//---------------------------------------------------------------------------
#endif
