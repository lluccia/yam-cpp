//---------------------------------------------------------------------------

#include <vcl.h>
#include <dateutils.hpp>
#include <dstring.h>
#include <process.h>
#include <stdio.h>
#pragma hdrstop

#include "unitYam.h"
#include "unitSobre.h"
#include "unitJogadores.h"
#include "unitRecordes.h"
#include "unitCliente.h"
#include "unitServidor.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmYam *frmYam;

//-----------------------------------------
//      DECLARA��O DE VARI�VEIS
//-----------------------------------------

 short int d[5]={6,6,6,6,6};  //array que armazena os valores dos dados
 bool l[5]= {false,false,false,false,false};
            //array que diz se os dados est�o travados
            // =true , o dado est� travado

//  vari�veis que dizem quais combina��es s�o poss�veis
 bool um = false;
 bool dois = false;
 bool tres = false;
 bool quatro = false;
 bool cinco = false;
 bool seis = false;
 bool quadra = false;
 bool full = false;
 bool seqmin = false;
 bool seqmax = false;
 bool min = false;
 bool max = false;
 bool yam = false;


 int Marcas[19][4];  //Diz quais marcas s�o poss�veis 0-� pode
                     //                               1-pode marcar
                     //                               2-pode riscar

 bool Marcado[19][4]; //Diz quais valores j� est�o marcados

 short int Score[19][4][4];
 short int Jogadores=1; //n� de jogadores
 short int Jogador=0;   //jogador atual
 short int Jogada=0;    //jogada atual
 short int TotalNosDados; //indica a soma dos valores dos dados
 struct Recorde
 {
  char Nome[10];
  int Score;
 } Rec[14];
 FILE *fp;

 char Nomes[4][10];

 byte buffer[300];

 bool Multiplayer=false;
 short int MPJogador=0;
 bool Servidor=false;
 AnsiString chat;

//---------------------------------------------------------------------------
__fastcall TfrmYam::TfrmYam(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmYam::btnJogarClick(TObject *Sender)
{
 if (!(l[0] && l[1] && l[2] && l[3] && l[4]))
 {
  sndPlaySound("dice.wav", SND_SYNC);
  Jogada=Jogada+1;
  lbJogadas->Caption=IntToStr(3-Jogada);
  if (Jogada==3) btnJogar->Enabled=false;
  JogaDados();
  OrdenaDados();
  DesenhaDados();
  Combinacoes();
  LimpaCartela();
  MarcacoesPossiveis();
  DesenhaPontos();
  lbTotalNosDados->Caption=IntToStr(TotalNosDados);
  if (Jogada==3)
  {
   DesenhaDados();
   for(int i=0;i<5;i++)
   {
    l[i]=false;
   }
  }

  if(Multiplayer)
  {
   buffer[0]=2;
   for(int i=0;i<5;i++)
   {
    buffer[i+1]=d[i];
    buffer[i+5+1]=l[i];
   }
   buffer[16]=MPJogador;
   if(Servidor)
    for(int c=0;c<Jogadores-1;c++)
     ServerSocket->Socket->Connections[c]->SendBuf(buffer,17);
   else
    ClientSocket->Socket->SendBuf(buffer,17);
  }
 }
}
//---------------------------------------------------------------------------
void TfrmYam::JogaDados()
{
 Randomize();
 int i;
 int t=RandomRange(5,10);
 for(int c=0;c<t;c++)
 {
  for(i=0;i<5;i++)
  {
   if (!l[i])
   {
    d[i]=RandomRange(1,7);
   }
  }
 }


 TotalNosDados=0;
 for(i=0;i<5;i++)
 {
  TotalNosDados=TotalNosDados+d[i];
 }

}
//---------------------------------------------------------------------------

void TfrmYam::OrdenaDados()
{
 int i,j,tmp;
 for(i=0;i<4;i++)
 {
  for(j=i+1;j<5;j++)
  {
   if(d[i]>d[j])
   {
    tmp=d[i];
    d[i]=d[j];
    d[j]=tmp;
    tmp=l[i];
    l[i]=l[j];
    l[j]=tmp;
   }
  }
 }
}
//---------------------------------------------------------------------------
void TfrmYam::DesenhaDados()
{
 int i;
 Dados->Canvas->Brush->Color=clGreen;
 Dados->Canvas->FillRect(Rect(0,0,
                               Dados->Width,Dados->Height));
 for(i=0;i<5;i++)
 {
  if ( !(l[i]) )
   imglistDados->Draw(Dados->Canvas,(i)*100,0,d[i],true);
  else
   imglistDadosL->Draw(Dados->Canvas,(i)*100,0,d[i],true);
 }
}
//---------------------------------------------------------------------------
void TfrmYam::Combinacoes()
{
 int i;

//limpa combina��es
 um = false;
 dois = false;
 tres = false;
 quatro = false;
 cinco = false;
 seis = false;
 quadra = false;
 full = false;
 seqmin = false;
 seqmax = false;
 min = false;
 max = false;
 yam = false;

 //checa um,dois,tres,quatro,cinco
 for (i=0;i<5;i++)
 {
  if (d[i]==1) um=true;
 }
 for(i=0;i<5;i++)
 {
  if (d[i]==2) dois=true;
 }
 for(i=0;i<5;i++)
 {
  if (d[i]==3) tres=true;
 }
 for(i=0;i<5;i++)
 {
  if (d[i]==4) quatro=true;
 }
 for(i=0;i<5;i++)
 {
  if (d[i]==5) cinco=true;
 }
 for(i=0;i<5;i++)
 {
  if (d[i]==6) seis=true;
 }

 //checa quadra
 if (d[0]==d[1] && d[0]==d[2] && d[0]==d[3] ||
      d[1]==d[2] && d[1]==d[3] && d[3]==d[4]) quadra=true;

 //checa full
 if (d[0]==d[1] && d[0]==d[2] && d[3]==d[4] && d[0]!=d[3] ||
      d[0]==d[1] && d[2]==d[3] && d[3]==d[4] && d[0]!=d[2]) full=true;

 //checa seq. m�nima
 if (d[0]==1 && d[1]==2 && d[2]==3 && d[3]==4 && d[4]==5) seqmin=true;

 //checa seq. m�xima
 if (d[0]==2 && d[1]==3 && d[2]==4 && d[3]==5 && d[4]==6) seqmax=true;

 //checa yam
 if (d[0]==d[1] && d[1]==d[2] && d[2]==d[3] && d[3]==d[4]) yam=true;

}
//----------------------------------------------------------------------------
void TfrmYam::MarcacoesPossiveis()
{
 int i,j;

//limpa a matriz de marca��es poss�veis
 for (i=0;i<4;i++)
 {
  for(j=0;j<19;j++)
  {
  Marcas[j][i]=0;
  }
 }

 //verifica quais valores est�o marcados para o jogador atual

 for(i=0;i<4;i++)
 {
  for(j=0;j<18;j++)
  {
   if (Score[j][i][Jogador]!=0) Marcado[j][i]=true;
   else Marcado[j][i]=false;
  }
 }


 //checa marca��es poss�veis

 //linha 1

 if (um)
 {
  if (!Marcado[0][0]) Marcas[0][0]=1;
  if (!Marcado[0][1] && Marcado[1][1]) Marcas[0][1]=1;
  if (!Marcado[0][2]) Marcas[0][2]=1;
  if (!Marcado[0][3] && Jogada==1) Marcas[0][3]=1;
 }

//linha 2

 if (dois)
 {
  if (!Marcado[1][0] && Marcado[0][0]) Marcas[1][0]=1;
  if (!Marcado[1][1] && Marcado[2][1]) Marcas[1][1]=1;
  if (!Marcado[1][2]) Marcas[1][2]=1;
  if (!Marcado[1][3] && Jogada==1) Marcas[1][3]=1;
 }

//linha 3

 if (tres)
 {
  if (!Marcado[2][0] && Marcado[1][0]) Marcas[2][0]=1;
  if (!Marcado[2][1] && Marcado[3][1]) Marcas[2][1]=1;
  if (!Marcado[2][2]) Marcas[2][2]=1;
  if (!Marcado[2][3] && Jogada==1) Marcas[2][3]=1;
 }

//linha 4

 if (quatro)
 {
  if (!Marcado[3][0] && Marcado[2][0]) Marcas[3][0]=1;
  if (!Marcado[3][1] && Marcado[4][1]) Marcas[3][1]=1;
  if (!Marcado[3][2]) Marcas[3][2]=1;
  if (!Marcado[3][3] && Jogada==1) Marcas[3][3]=1;
 }

//linha 5

 if (cinco)
 {
  if (!Marcado[4][0] && Marcado[3][0]) Marcas[4][0]=1;
  if (!Marcado[4][1] && Marcado[5][1]) Marcas[4][1]=1;
  if (!Marcado[4][2]) Marcas[4][2]=1;
  if (!Marcado[4][3] && Jogada==1) Marcas[4][3]=1;
 }

//linha 6

 if (seis)
 {
  if (!Marcado[5][0] && Marcado[4][0]) Marcas[5][0]=1;
  if (!Marcado[5][1] && Marcado[9][1]) Marcas[5][1]=1;
  if (!Marcado[5][2]) Marcas[5][2]=1;
  if (!Marcado[5][3] && Jogada==1) Marcas[5][3]=1;
 }

//linha Quadra

 if (quadra)
 {
  if (!Marcado[9][0] && Marcado[5][0]) Marcas[9][0]=1;
  if (!Marcado[9][1] && Marcado[10][1]) Marcas[9][1]=1;
  if (!Marcado[9][2]) Marcas[9][2]=1;
  if (!Marcado[9][3] && Jogada==1) Marcas[9][3]=1;
 }

//linha Full

 if (full)
 {
  if (!Marcado[10][0] && Marcado[9][0]) Marcas[10][0]=1;
  if (!Marcado[10][1] && Marcado[11][1]) Marcas[10][1]=1;
  if (!Marcado[10][2]) Marcas[10][2]=1;
  if (!Marcado[10][3] && Jogada==1) Marcas[10][3]=1;
 }

//linha S-

 if (seqmin)
 {
  if (!Marcado[11][0] && Marcado[10][0]) Marcas[11][0]=1;
  if (!Marcado[11][1] && Marcado[12][1]) Marcas[11][1]=1;
  if (!Marcado[11][2]) Marcas[11][2]=1;
  if (!Marcado[11][3] && Jogada==1) Marcas[11][3]=1;
 }

//linha S+

 if (seqmax)
 {
  if (!Marcado[12][0] && Marcado[11][0]) Marcas[12][0]=1;
  if (!Marcado[12][1] && Marcado[13][1]) Marcas[12][1]=1;
  if (!Marcado[12][2]) Marcas[12][2]=1;
  if (!Marcado[12][3] && Jogada==1) Marcas[12][3]=1;
 }

//linha Min

 if (!Marcado[13][0] && Marcado[12][0]) Marcas[13][0]=1;
 if (!Marcado[13][1] && Marcado[14][1] && TotalNosDados<Score[14][1][Jogador]) Marcas[13][1]=1;
 if (!Marcado[13][2])
  if (!Marcado[14][2]) Marcas[13][2]=1;
   else if (TotalNosDados<Score[14][2][Jogador]) Marcas[13][2]=1;
 if (!Marcado[13][3] && Jogada==1)
  if (!Marcado[14][3]) Marcas[13][3]=1;
   else if (TotalNosDados<Score[14][3][Jogador]) Marcas[13][3]=1;

//linha Max

 if (!Marcado[14][0] && Marcado[13][0] && TotalNosDados>Score[13][0][Jogador]) Marcas[14][0]=1;
 if (!Marcado[14][1] && Marcado[15][1] ) Marcas[14][1]=1;
 if (!Marcado[14][2])
  if (!Marcado[13][2]) Marcas[14][2]=1;
   else if (TotalNosDados>Score[13][2][Jogador]) Marcas[14][2]=1;
 if (!Marcado[14][3] && Jogada==1)
  if (!Marcado[13][3]) Marcas[14][3]=1;
   else if (TotalNosDados>Score[13][3][Jogador]) Marcas[14][3]=1;

//linha Yam

 if (yam)
 {
  if (!Marcado[15][0] && Marcado[14][0]) Marcas[15][0]=1;
  if (!Marcado[15][1]) Marcas[15][1]=1;
  if (!Marcado[15][2]) Marcas[15][2]=1;
  if (!Marcado[15][3] && Jogada==1) Marcas[15][3]=1;
 }


//checa riscos poss�veis

//coluna desce

 if(!Marcado[0][0] && Marcas[0][0]!=1) Marcas[0][0]=2;
 for(i=1;i<16;i++)
 {
  if(!(i>5 && i<10))
   if(!Marcado[i][0] && Marcado[i-1][0] && Marcas[i][0]!=1) Marcas[i][0]=2;
  if(i==9 && !Marcado[i][0] && Marcado[5][0] && Marcas[i][0]!=1) Marcas[i][0]=2;
 }

//coluna sobe

 if(!Marcado[15][1] && Marcas[15][1]!=1) Marcas[15][1]=2;
 for(i=14;i>=0;i--)
 {
  if(!(i>4 && i<9))
   if(!Marcado[i][1] && Marcado[i+1][1] && Marcas[i][1]!=1) Marcas[i][1]=2;
  if(i==5 && !Marcado[i][1] && Marcado[9][1] && Marcas[i][1]!=1) Marcas[i][1]=2;
 }

//coluna desordem

 for(i=0;i<16;i++)
 {
  if(!(i>5 && i<9))
   if(!Marcado[i][2] && Marcas[i][2]!=1) Marcas[i][2]=2;
 }

//coluna seco

 for(i=0;i<16;i++)
 {
  if(!(i>5 && i<9))
   if(!Marcado[i][3] && Marcas[i][3]!=1) Marcas[i][3]=2;
 }

//desenha as indica��es de marcas na cartela
 Cartela->Canvas->Brush->Color=clHotLight;

 for (i=0;i<4;i++)
 {
  for(j=0;j<19;j++)
  {
  if (Marcas[j][i]==1) Cartela->Canvas->FillRect(Rect(42+42*i,22+21*j,
                                                      83+42*i,42+21*j));
  }
 }

//desenha as indica��es de riscos na cartela
 Cartela->Canvas->Brush->Color=clRed;

 for (i=0;i<4;i++)
 {
  for(j=0;j<19;j++)
  {
  if (Marcas[j][i]==2) Cartela->Canvas->FillRect(Rect(42+42*i,22+21*j,
                                                      83+42*i,42+21*j));
  }
 }
}
//----------------------------------------------------------------------------
void TfrmYam::LimpaCartela()
{
 int i,j;
 Cartela->Canvas->Brush->Color=clWhite;
 for (i=0;i<4;i++)
 {
  for(j=0;j<18;j++)
  {
  if (!(j>5 && j<9 || j>15)) Cartela->Canvas->FillRect(Rect(42+42*i,22+21*j,
                                                    83+42*i,42+21*j));
  }
 }

 Cartela->Canvas->Brush->Color=clSilver;
 for (i=0;i<4;i++)
 {
  for(j=0;j<18;j++)
  {
  if ((j>5 && j<9 || j>15)) Cartela->Canvas->FillRect(Rect(42+42*i,22+21*j,
                                                    83+42*i,42+21*j));
  }
 }
 Cartela->Canvas->FillRect(Rect(42+42*2,22+21*18,
                                83+42*3,42+21*18));

}

//----------------------------------------------------------------------------
void TfrmYam::DesenhaPontos()
{
 int i,j;

 Cartela->Canvas->Brush->Color=clWhite;
 for (i=0;i<4;i++)
 {
  for(j=0;j<18;j++)
  {
  if (!(j>5 && j<9 || j>15) && Score[j][i][Jogador]!=0) Cartela->Canvas->TextOutA(57+42*i,25+21*j,
                                                   IntToStr(Score[j][i][Jogador]));
  if (!(j>5 && j<9 || j>15) && Score[j][i][Jogador]<0) Cartela->Canvas->TextOutA(57+42*i,25+21*j,
                                                   "---");
  }
 }

 Cartela->Canvas->Brush->Color=clSilver;
 for (i=0;i<4;i++)
 {
  for(j=0;j<18;j++)
  {
  if ((j>5 && j<9 || j>15) && Score[j][i][Jogador]!=0) Cartela->Canvas->TextOutA(57+42*i,25+21*j,
                                                  IntToStr(Score[j][i][Jogador]));
  }
 }
 if (Score[18][3][Jogador]!=0) Cartela->Canvas->TextOutA(52+42*2,25+21*18,IntToStr(Score[18][3][Jogador]));

}
//---------------------------------------------------------------------------
void __fastcall TfrmYam::DadosMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
 if ( Jogada<3 && Jogada!=0 && (!Multiplayer || Multiplayer && Jogador==MPJogador))
 {
  if ( Button == mbLeft )
  {
   int ymin=0;
   int ymax=100;
   if (Y > ymin && Y < ymax)
   {
    for (int i=0;i<5;i++)
    {
     int xmin=(i*100);
     int xmax=100+(i*100);
     if ( X > xmin && X < xmax)
     {
      l[i]=!l[i];
     }
    }
    DesenhaDados();
   }
  }
  if(Multiplayer)
  {
   buffer[0]=3; //manda os locks para o cliente/servidor
   for(int i=0;i<5;i++)
   {
    buffer[i+1]=l[i];
   }
   buffer[6]=MPJogador;
   if(Servidor)
    for(int c=0;c<Jogadores-1;c++)
     ServerSocket->Socket->Connections[c]->SendBuf(buffer,7);
   else
    ClientSocket->Socket->SendBuf(buffer,7);
  }
 }
}

//---------------------------------------------------------------------------

void TfrmYam::MarcaPonto(int i,int j)
{
 int pontos=0;
 int c;

//se nas linhas de 1 a 6
 if (j>=0 && j<6 && Marcas[j][i]==1)
 {
  for (c=0;c<5;c++)
  {
   if (d[c]==j+1) pontos=pontos+j+1;
  }
 }

//se quadra
 if (j==9 && Marcas[j][i]==1)
 {
  if(d[0]==d[1])
  {
   for(c=0;c<4;c++) pontos=pontos+d[c];
  }
  else
  {
   for(c=1;c<5;c++) pontos=pontos+d[c];
  }
  pontos=pontos+20;
 }

//se full
 if (j==10 && Marcas[j][i]==1)
 {
  for(c=0;c<5;c++) pontos=pontos+d[c];
  pontos=pontos+30;
 }

//se seqmin
 if (j==11 && Marcas[j][i]==1)
 {
  pontos=50;
 }

//se seqmax
 if (j==12 && Marcas[j][i]==1)
 {
  pontos=60;
 }

//se min ou max ou yam
 if ((j==13 || j==14 || j==15)&& Marcas[j][i]==1)
 {
  for(c=0;c<5;c++) pontos=pontos+d[c];
  if (j==15) pontos=pontos+50;
 }

//se riscar

 if (((j>=0 && j<=5) || (j>=9 && j<=15)) && Marcas[j][i]==2)
 {
  pontos=-1;
 }

 Score[j][i][Jogador]=pontos;

 CalculaTotais();

 lbJogador1->Caption=IntToStr(Score[18][3][0]);
 lbJogador2->Caption=IntToStr(Score[18][3][1]);
 lbJogador3->Caption=IntToStr(Score[18][3][2]);
 lbJogador4->Caption=IntToStr(Score[18][3][3]);
}

//---------------------------------------------------------------------------
void TfrmYam::CalculaTotais()
{
 for (int i=0;i<4;i++)
 {
  Score[6][i][Jogador]=0;
  for (int j=0;j<6;j++)
  {
   if (Score[j][i][Jogador]>0)
   {
    Score[6][i][Jogador]=Score[6][i][Jogador]+Score[j][i][Jogador];
   }
  }

  Score[16][i][Jogador]=0;
  for (int j=9;j<16;j++)
  {
   if (Score[j][i][Jogador]>0)
   {
    Score[16][i][Jogador]=Score[16][i][Jogador]+Score[j][i][Jogador];
   }
  }

  if (Score[6][i][Jogador]>=60)
   Score[7][i][Jogador]=30;

  Score[8][i][Jogador]=Score[6][i][Jogador]+Score[7][i][Jogador];
  Score[17][i][Jogador]=Score[8][i][Jogador]+Score[16][i][Jogador];
 }

 Score[18][3][Jogador]=Score[17][0][Jogador]+
                       Score[17][1][Jogador]+
                       Score[17][2][Jogador]+
                       Score[17][3][Jogador];


}

//---------------------------------------------------------------------------

void __fastcall TfrmYam::CartelaMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
 if ((Button==mbLeft) && (!Multiplayer || ( Multiplayer && (MPJogador==Jogador))))
 {
  int Mi,Mj;
  Mi = X / 42 - 1;
  Mj = Y / 21 - 1;
  if(Mi>=0 && Mj>=0 && (Mj<6 || (Mj>8 && Mj<16)) && !Marcado[Mj][Mi] && Marcas[Mj][Mi]>0)
  {
   if (Marcas[Mj][Mi]==1) sndPlaySound("marca.wav",SND_ASYNC);
   if (Marcas[Mj][Mi]==2) sndPlaySound("risca.wav",SND_ASYNC);
   MarcaPonto(Mi,Mj);
   btnJogar->Enabled=true;
   LimpaCartela();
   DesenhaPontos();
   frmYam->Refresh();

   if (Multiplayer)
   {
    btnJogar->Enabled=false;
    buffer[0]=4;
    if (Marcas[Mj][Mi]==1) buffer[1]=1; //marca
    if (Marcas[Mj][Mi]==2) buffer[1]=2; //risca
    buffer[2]=Mj;
    buffer[3]=Mi;
    buffer[4]=MPJogador;
    if(Servidor)
    {
     for(int c=0;c<Jogadores-1;c++)
      ServerSocket->Socket->Connections[c]->SendBuf(buffer,5);
    }
    else
    {
     ClientSocket->Socket->SendBuf(buffer,5);
    }
   }

   if (!Multiplayer || Multiplayer && Jogador==MPJogador)
   {
    if (Jogadores>1) Sleep(1500);
    Jogador++;
    if(Jogador==Jogadores) Jogador=0;
    HLJogador();
    Jogada=0;
    lbJogadas->Caption=IntToStr(3-Jogada);
    for(int i=0;i<5;i++) l[i]=false;
    DesenhaDados();
    for(int i=0;i<4;i++)
     for(int j=0;j<19;j++)
      Marcas[j][i]=0;
    ChecaFimDeJogo();
    LimpaCartela();
    DesenhaPontos();
   }
  }
 }
}
//---------------------------------------------------------------------------

void TfrmYam::Iniciar()
{
 if(frmJogadores->ShowModal()==mrOk)
 {
  ZeraScore();
  Jogador=0;
  Jogada=0;
  Jogadores=frmJogadores->udJogadores->Position;
  btnJogar->Enabled=true;
  lblJogador1->Caption="1."+frmJogadores->Edit1->Text+":";
  lblJogador2->Caption="2."+frmJogadores->Edit2->Text+":";
  lblJogador3->Caption="3."+frmJogadores->Edit3->Text+":";
  lblJogador4->Caption="4."+frmJogadores->Edit4->Text+":";
  lbJogadas->Caption="3";

  lblJogador2->Visible=false;
  lblJogador3->Visible=false;
  lblJogador4->Visible=false;
  lbJogador2->Visible=false;
  lbJogador3->Visible=false;
  lbJogador4->Visible=false;

  if (Jogadores > 1)
  {
   lblJogador2->Visible=true;
   lbJogador2->Visible=true;
   if (Jogadores > 2)
   {
    lblJogador3->Visible=true;
    lbJogador3->Visible=true;
    if (Jogadores > 3)
    {
     lblJogador4->Visible=true;
     lbJogador4->Visible=true;
    }
   }
  }
 }
 HLJogador();
}
//---------------------------------------------------------------------------
bool TfrmYam::ChecaFimDeJogo()
{
 bool fim1=true,fim2=true,fim=true;
 for(int i=0;i<4;i++)
 {
  for(int j=0;j<6;j++)
  {
   if(Score[j][i][Jogadores-1]==0) fim1=false;
  }
  for(int j=9;j<16;j++)
  {
   if(Score[j][i][Jogadores-1]==0) fim2=false;
  }
 }
 if(fim1==false || fim2==false) fim=false;
 if(fim)
 {
  FimDeJogo();
  return true;
 }
 else
 {
  return false;
 }
}
//---------------------------------------------------------------------------
void TfrmYam::FimDeJogo()
{
 btnJogar->Enabled=false;
 btnEnviar->Enabled=false;
 edChat->Enabled=false;
 ChecaRecordes();
 frmRecordes->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::mNovoJogoClick(TObject *Sender)
{
 Iniciar();
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::mSairClick(TObject *Sender)
{
 Application->Terminate();
}
//---------------------------------------------------------------------------

void TfrmYam::ZeraScore()
{
 for(int k=0;k<4;k++)
  for(int i=0;i<4;i++)
   for(int j=0;j<19;j++)
    Score[j][i][k]=0;

 lbJogador1->Caption="0";
 lbJogador2->Caption="0";
 lbJogador3->Caption="0";
 lbJogador4->Caption="0";
 LimpaCartela();
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::SobreClick(TObject *Sender)
{
 frmSobre->ShowModal();
}
//---------------------------------------------------------------------------


void __fastcall TfrmYam::RegrasClick(TObject *Sender)
{
 ShellExecute(frmSobre,"open","Yam.hlp",NULL,NULL,0);
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::mRecordesClick(TObject *Sender)
{
 frmRecordes->ShowModal();
}

void  TfrmYam::ChecaRecordes()
{
 FILE *fp;

 fp=fopen("high.dat","r+b");
 fread(Rec,10*sizeof(Recorde),1,fp);

 for(int c=10;c<14;c++)
 {
  Rec[c].Score=Score[18][3][c-10];
 }
 if(Multiplayer)
 {
  for(int c=10;c<14;c++)
  {
   strcpy(Rec[c].Nome,Nomes[c-10]);
  }
 }
 else
 {
  strcpy(Rec[10].Nome,frmJogadores->Edit1->Text.c_str());
  strcpy(Rec[11].Nome,frmJogadores->Edit2->Text.c_str());
  strcpy(Rec[12].Nome,frmJogadores->Edit3->Text.c_str());
  strcpy(Rec[13].Nome,frmJogadores->Edit4->Text.c_str());
 }
 //bubblesort

 for (int i=0; i<13; i++)
 {
  for (int j=0; j<13-i; j++)
    if (Rec[j+1].Score > Rec[j].Score)
    {
      Recorde tmp;
      tmp = Rec[j];
      Rec[j] = Rec[j+1];
      Rec[j+1] = tmp;
    }
 }
 rewind(fp);
 fwrite(Rec,10*sizeof(Recorde),1,fp);

 fclose(fp);
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::FormCreate(TObject *Sender)
{
 DesenhaDados();
}
//---------------------------------------------------------------------------

void TfrmYam::HLJogador()
{
 lblJogador1->Color=clGreen;
 lblJogador2->Color=clGreen;
 lblJogador3->Color=clGreen;
 lblJogador4->Color=clGreen;
 if (Jogador==0) lblJogador1->Color=clBlue;
 if (Jogador==1) lblJogador2->Color=clBlue;
 if (Jogador==2) lblJogador3->Color=clBlue;
 if (Jogador==3) lblJogador4->Color=clBlue;
}
void __fastcall TfrmYam::edChatKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
 if (Key == VK_RETURN) btnEnviarClick(edChat);
}
//---------------------------------------------------------------------------



void __fastcall TfrmYam::mServidorClick(TObject *Sender)
{
 ServerSocket->Close();
 frmServidor->lbJogadores->Clear();
 frmServidor->sbServidor->SimpleText="";
 frmServidor->btnCriar->Enabled=true;
 if (frmServidor->ShowModal()==mrCancel)
 {
  ServerSocket->Close();
  frmServidor->lbJogadores->Clear();
  frmServidor->sbServidor->SimpleText="";
 }
 else IniciarMultiplayer();
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::mClienteClick(TObject *Sender)
{
 ClientSocket->Close();
 frmCliente->ShowModal();
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::ClientSocketConnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
 frmCliente->sbCliente->SimpleText="Conectado!";
 frmCliente->btnConectar->Enabled=false;
 buffer[0]=0;
 StrCopy(&buffer[1],frmCliente->edNome->Text.c_str());
 buffer[10]=0;
 ClientSocket->Socket->SendBuf(buffer,11);
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::ServerSocketListen(TObject *Sender,
      TCustomWinSocket *Socket)
{
 StrCopy(Nomes[0],frmServidor->edNome->Text.c_str());
 Jogadores=1;
 frmServidor->lbJogadores->Items->SetText(Nomes[0]);
 frmServidor->btnCriar->Enabled=false;
 frmServidor->sbServidor->SimpleText="Jogo Criado! Aguardando Jogadores!";
 Servidor=true;
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::ClientSocketConnecting(TObject *Sender,
      TCustomWinSocket *Socket)
{
 frmCliente->sbCliente->SimpleText="Tentando Conectar!";        
}
//---------------------------------------------------------------------------


void __fastcall TfrmYam::ServerSocketClientRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
 Socket->ReceiveBuf(buffer,Socket->ReceiveLength());
 if (buffer[0]==0) // o cliente mandou o Nome
 {
  if (ServerSocket->Socket->ActiveConnections==4)
  {
   Socket->Close();
  }
  else
  {
   Jogadores=ServerSocket->Socket->ActiveConnections+1;
   StrCopy(Nomes[Jogadores-1],&buffer[1]);
   frmServidor->lbJogadores->Clear();
   char LBNomes[48];
   int x=0;
   for(int i=0;i<Jogadores;i++) //loop para montar o listbox
   {
    for(int c=0;Nomes[i][c]!=0;c++)
    {
     LBNomes[x++]=Nomes[i][c];
    }
    LBNomes[x++]='\r';
    LBNomes[x++]='\n';
   }
   LBNomes[x]=0;
   frmServidor->lbJogadores->Items->SetText(LBNomes);
   frmServidor->btnIniciar->Enabled=true;
   frmServidor->btnIniciar->Default=true;

   buffer[0]=0;
   StrCopy(&buffer[1],frmServidor->lbJogadores->Items->GetText());
   for (short int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
   {
    buffer[50]=i+1;
    ServerSocket->Socket->Connections[i]->SendBuf(buffer,51);
   }
  }
 }
 if(buffer[0]==2) // o cliente mandou os dados e locks
 {
  for(int i=0;i<5;i++)
  {
   d[i]=buffer[i+1];
   l[i]=buffer[i+5+1];
  }
  sndPlaySound("dice.wav", SND_SYNC);
  Jogada=Jogada+1;
  lbJogadas->Caption=IntToStr(3-Jogada);
  TotalNosDados=0;
  for(int i=0;i<5;i++)
  {
   TotalNosDados=TotalNosDados+d[i];
  }
  lbTotalNosDados->Caption=IntToStr(TotalNosDados);
  DesenhaDados();
  Combinacoes();
  LimpaCartela();
  MarcacoesPossiveis();
  DesenhaPontos();


  if (Jogada==3)
  {
   DesenhaDados();
   for(int i=0;i<5;i++)
   {
    l[i]=false;
   }
  }
  for (short int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
   ServerSocket->Socket->Connections[i]->SendBuf(buffer,16);
 }
 if(buffer[0]==3) // o cliente mandou os locks
 {
  for(int i=0;i<5;i++)
  {
   l[i]=buffer[i+1];
  }
  DesenhaDados();
  for (short int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
   ServerSocket->Socket->Connections[i]->SendBuf(buffer,7);
 }
 if(buffer[0]==4) // o cliente mandou o score
 {
  MarcaPonto(buffer[3],buffer[2]);
  LimpaCartela();
  DesenhaPontos();
  lbJogador1->Caption=IntToStr(Score[18][3][0]);
  lbJogador2->Caption=IntToStr(Score[18][3][1]);
  lbJogador3->Caption=IntToStr(Score[18][3][2]);
  lbJogador4->Caption=IntToStr(Score[18][3][3]);
  frmYam->Refresh();
  if (buffer[1]==1) sndPlaySound("marca.wav",SND_ASYNC); //marca
  if (buffer[1]==2) sndPlaySound("risca.wav",SND_ASYNC); //risca
  for (short int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
   ServerSocket->Socket->Connections[i]->SendBuf(buffer,5);
  Sleep(1500);
  Jogador++;
  if(Jogador==Jogadores) Jogador=0;
  if(MPJogador==Jogador) btnJogar->Enabled=true;
  HLJogador();
  Jogada=0;
  lbJogadas->Caption=IntToStr(3-Jogada);
  for(int i=0;i<5;i++) l[i]=false;
  DesenhaDados();
  for(int i=0;i<4;i++)
   for(int j=0;j<19;j++)
    Marcas[j][i]=0;
  ChecaFimDeJogo();
  LimpaCartela();
  DesenhaPontos();
 }

 if (buffer[0]==5) //o cliente mandou chat
 {
  chat="<"+AnsiString(Nomes[buffer[1]])+">: "+AnsiString((const char *)&buffer[2]);
  memoChat->Lines->Add(chat);
  for (short int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
   ServerSocket->Socket->Connections[i]->SendBuf(buffer,255);
 }
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::ClientSocketRead(TObject *Sender,
      TCustomWinSocket *Socket)
{
 Socket->ReceiveBuf(buffer,Socket->ReceiveLength());
 if (buffer[0]==0) // o servidor mandou os Nomes
 {
  MPJogador=buffer[50];
  char LBNomes[49];
  StrCopy(LBNomes,&buffer[1]);
  frmCliente->lbJogadores->Items->SetText(LBNomes);
  for (int i=0;i<frmCliente->lbJogadores->Items->Count;i++)
   StrCopy(Nomes[i],frmCliente->lbJogadores->Items->Strings[i].c_str());
 }
 if (buffer[0]==1) // inicio de jogo
 {
  Multiplayer=true;
  frmCliente->ModalResult=mrOk;
  ZeraScore();
  Jogador=0;
  Jogada=0;
  Jogadores=frmCliente->lbJogadores->Items->Count;
  lblJogador1->Caption="1."+frmCliente->lbJogadores->Items->Strings[0]+":";
  lblJogador2->Caption="2."+frmCliente->lbJogadores->Items->Strings[1]+":";
  lbJogadas->Caption="3";

  lblJogador2->Visible=false;
  lblJogador3->Visible=false;
  lblJogador4->Visible=false;
  lbJogador2->Visible=false;
  lbJogador3->Visible=false;
  lbJogador4->Visible=false;

  if (Jogadores > 1)
  {
   lblJogador2->Visible=true;
   lbJogador2->Visible=true;
   if (Jogadores > 2)
   {
    lblJogador3->Caption="3."+frmCliente->lbJogadores->Items->Strings[2]+":";
    lblJogador3->Visible=true;
    lbJogador3->Visible=true;
    if (Jogadores > 3)
    {
     lblJogador4->Caption="4."+frmCliente->lbJogadores->Items->Strings[3]+":";
     lblJogador4->Visible=true;
     lbJogador4->Visible=true;
    }
   }
  }
  HLJogador();
  btnEnviar->Enabled=true;
  edChat->Enabled=true;
 }
 if(buffer[0]==2 && MPJogador!=buffer[16]) // o servidor mandou os dados e locks
 {
  for(int i=0;i<5;i++)
  {
   d[i]=buffer[i+1];
   l[i]=buffer[i+5+1];
  }
  sndPlaySound("dice.wav", SND_SYNC);
  Jogada=Jogada+1;
  lbJogadas->Caption=IntToStr(3-Jogada);
  TotalNosDados=0;
  for(int i=0;i<5;i++)
  {
   TotalNosDados=TotalNosDados+d[i];
  }
  lbTotalNosDados->Caption=IntToStr(TotalNosDados);
  DesenhaDados();
  Combinacoes();
  LimpaCartela();
  MarcacoesPossiveis();
  DesenhaPontos();

  if (Jogada==3)
  {
   DesenhaDados();
   for(int i=0;i<5;i++)
   {
    l[i]=false;
   }
  }
 }
 if(buffer[0]==3 && MPJogador!=buffer[6]) // o servidor mandou os locks
 {
  for(int i=0;i<5;i++)
  {
   l[i]=buffer[i+1];
  }
  DesenhaDados();
 }
 if(buffer[0]==4 && MPJogador!=buffer[4]) // o servidor mandou o score
 {
  MarcaPonto(buffer[3],buffer[2]);
  LimpaCartela();
  DesenhaPontos();
  CalculaTotais();
  lbJogador1->Caption=IntToStr(Score[18][3][0]);
  lbJogador2->Caption=IntToStr(Score[18][3][1]);
  lbJogador3->Caption=IntToStr(Score[18][3][2]);
  lbJogador4->Caption=IntToStr(Score[18][3][3]);
  frmYam->Refresh();
  if (buffer[1]==1) sndPlaySound("marca.wav",SND_ASYNC); //marca
  if (buffer[1]==2) sndPlaySound("risca.wav",SND_ASYNC); //risca
  Sleep(1500);
  Jogador++;
  if(Jogador==Jogadores) Jogador=0;
  if(MPJogador==Jogador)
   btnJogar->Enabled=true;
  else
   btnJogar->Enabled=false;
  HLJogador();
  Jogada=0;
  lbJogadas->Caption=IntToStr(3-Jogada);
  for(int i=0;i<5;i++) l[i]=false;
  DesenhaDados();
  for(int i=0;i<4;i++)
   for(int j=0;j<19;j++)
    Marcas[j][i]=0;
  ChecaFimDeJogo();
  LimpaCartela();
  DesenhaPontos();
 }
 if (buffer[0]==5) //o servidor mandou chat
 {
  chat="<"+AnsiString(Nomes[buffer[1]])+">: "+AnsiString((const char *)&buffer[2]);
  memoChat->Lines->Add(chat);
 }

}
//---------------------------------------------------------------------------
void TfrmYam::IniciarMultiplayer()
{
 Multiplayer=true;
 buffer[0]=1;
 for (int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
  ServerSocket->Socket->Connections[i]->SendBuf(buffer,1);
 ZeraScore();
 Jogador=0;
 Jogada=0;
 Jogadores=frmServidor->lbJogadores->Items->Count;
 btnJogar->Enabled=true;
 lblJogador1->Caption="1."+frmServidor->lbJogadores->Items->Strings[0]+":";
 lblJogador2->Caption="2."+frmServidor->lbJogadores->Items->Strings[1]+":";

 lbJogadas->Caption="3";

 lblJogador2->Visible=false;
 lblJogador3->Visible=false;
 lblJogador4->Visible=false;
 lbJogador2->Visible=false;
 lbJogador3->Visible=false;
 lbJogador4->Visible=false;

 if (Jogadores > 1)
 {
  lblJogador2->Visible=true;
  lbJogador2->Visible=true;
  if (Jogadores > 2)
  {
   lblJogador3->Caption="3."+frmServidor->lbJogadores->Items->Strings[2]+":";
   lblJogador3->Visible=true;
   lbJogador3->Visible=true;
   if (Jogadores > 3)
   {
    lblJogador4->Caption="4."+frmServidor->lbJogadores->Items->Strings[3]+":";
    lblJogador4->Visible=true;
    lbJogador4->Visible=true;
   }
  }
 }
 HLJogador();
 if(Servidor) btnJogar->Enabled=true;
 edChat->Enabled=true;
 btnEnviar->Enabled=true;
}
void __fastcall TfrmYam::btnEnviarClick(TObject *Sender)
{
 if (edChat->Text!="")
 {
  buffer[0]=5;
  buffer[1]=MPJogador;
  StrCopy(&buffer[2],edChat->Text.c_str());
  edChat->Text="";
  if(Servidor)
  {
   for (int i=0;i<ServerSocket->Socket->ActiveConnections;i++)
    ServerSocket->Socket->Connections[i]->SendBuf(buffer,255);
   chat="<"+AnsiString(Nomes[buffer[1]])+">: "+AnsiString((const char *)&buffer[2]);
   memoChat->Lines->Add(chat);
  }
  else
   ClientSocket->Socket->SendBuf(buffer,255);
 }
}
//---------------------------------------------------------------------------

void __fastcall TfrmYam::ClientSocketDisconnect(TObject *Sender,
      TCustomWinSocket *Socket)
{
 frmCliente->sbCliente->SimpleText="Desconectado! O Servidor est� lotado!";
 frmCliente->btnConectar->Enabled=true;        
}
//---------------------------------------------------------------------------

