//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "unitServidor.h"
#include "unitYam.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmServidor *frmServidor;
//---------------------------------------------------------------------------
__fastcall TfrmServidor::TfrmServidor(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TfrmServidor::btnCriarClick(TObject *Sender)
{
 if (edNome->Text!="")
 {
  frmYam->ServerSocket->Port=StrToInt(edPorta->Text);
  frmYam->ServerSocket->Open();
 }
 else
  Application->MessageBoxA("Voc� deve digitar um nome!","Erro!",MB_OK);
}
//---------------------------------------------------------------------------

