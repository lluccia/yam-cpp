//---------------------------------------------------------------------------

#include <vcl.h>
#include <shellapi.h>
#pragma hdrstop

#include "unitSobre.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TfrmSobre *frmSobre;
//---------------------------------------------------------------------------
__fastcall TfrmSobre::TfrmSobre(TComponent* Owner)
        : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TfrmSobre::lblMailtoClick(TObject *Sender)
{
 ShellExecute(frmSobre,"open","mailto:lluccia@uol.com.br",NULL,NULL,0);
}
//---------------------------------------------------------------------------

void __fastcall TfrmSobre::btnOkClick(TObject *Sender)
{
 frmSobre->Close();         
}
//---------------------------------------------------------------------------


